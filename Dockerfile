FROM openjdk


WORKDIR /spring-petclinic

ENTRYPOINT ["java", "-jar", "spring-app.jar"]