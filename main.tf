provider "aws" {
  region = "us-east-1"
}

terraform {
    backend "s3" {
      bucket = "tfstatejoelsonmvp"
      key = "terraform.tfstate"
      region = "us-east-1"
    }
}

resource "aws_key_pair" "projeto-key" {
  key_name   = "projeto-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDb9KBGLGX3p9RAq+G5n1JbZiUYQqDohsVXDszkt+QSbedKVgpnJgGV/gad8ulq9/zDB1zcJWGbpEngn8m8+X/64x8Tgu+qWKLsHowY+JQuG7gaiQDv8X+Q/mMhF7Bqh6njdpyTshjAaKzAfXfQ+GXlxtYUsl3bKDTbqvfKCjyuOYsn0zN4b8CPC8DtOo80Bcs1yCuR7d1Wc7Vfu7xIM3ls0bMogz7CajIFasjV745VP7VXs0GghdiUsA6J27YQWmxXtL3dw+frjbH+2n83niun2GfQvcxwjCNfilJu6piRoAV+9gmYD5KXr7qhdxhqV2rhUD3i4Q0KbfGvE8X0gh6OPa48kellKEALNzN5h85Dwad1nCm9FLrLTRzUuRRPNJnCZ51rncV+B9CILVfve/Lj/vTMAlwJKzwdtoXDVWbueLptnM0R4PbRRSgjtIdRijyhYWnA8ibjneDk6aCb9e4MJniKrxgimjBCVymRu2nYW2bbAmJhdBCbaolLLmR0778= root@hd-sup-01"
}

resource "aws_security_group" "projeto-sg" {

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }   

}

resource "aws_instance" "ec2projeto" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t2.micro"
  count         = 1
  key_name      = "projeto-key"
  user_data     = file("userdata.tpl")

  tags = {
    name = "ec2Projeto"
    type = "ProjetoEC2"
  }
  security_groups = ["${aws_security_group.projeto-sg.name}"]
}


